*** Life<FredkinCell> 6x11 ***

Generation = 0, Population = 20.
---0------0
000---0--00
0-00----0--
---00------
--000------
-0--0----0-

Generation = 3, Population = 33.
2--0--00--1
001--0--01-
-2--0-2----
210--20-00-
--1332-0--0
0-1-1--10-0

*** Life<FredkinCell> 9x12 ***

Generation = 0, Population = 11.
---000------
0-----------
------------
-0----------
0----------0
0----0--0---
------------
------------
------0-----

Generation = 1, Population = 30.
0-01-10-----
-0-000------
00----------
--0--------0
1----0--0-0-
10--0-00-0-0
0----0--0---
------0-----
-----0-0----

Generation = 2, Population = 36.
-01-0-10----
010---------
1--000-----0
---0-0--0---
2-0------0-0
2100--11--0-
1-----0----0
0----0--0---
----0-0-0---

Generation = 3, Population = 49.
------210---
-2---000---0
2-----0-0-0-
---10100----
3010-000--0-
3-1100-20--0
--00--1-0---
10----0010-0
0--0-0--10--

Generation = 4, Population = 30.
-0-----210-0
--0-01--0---
---000------
-0---2------
--21-111-0--
412------0--
------2-----
-1----------
1-0-------00

Generation = 5, Population = 45.
0---012321--
---0--0-1--0
-0-1110-0---
0------0-0--
30-2---2-10-
5----0-2010-
1-0--0-0-0--
------0---00
-0-0-----011

*** Life<FredkinCell> 8x9 ***

Generation = 0, Population = 7.
-------0-
------0--
---------
------00-
--0------
---------
---------
--00-----

Generation = 4, Population = 16.
--00---2-
--1---2-2
-------3-
-------21
0-----01-
--12--2--
---------
2--------

Generation = 8, Population = 21.
---2-----
0-201----
--3------
0--21-33-
--1-3---2
-------1-
-------0-
---22-200

*** Life<FredkinCell> 26x13 ***

Generation = 0, Population = 12.
-------------
-------------
-------------
-------------
-------------
-------------
0------------
---0---------
--0-------0--
-------------
-------------
-------0----0
---0---------
---0---------
-----00------
-------------
-------------
-------------
-------------
-------------
--------0----
----0--------
-------------
-------------
-------------
-------------

Generation = 3, Population = 98.
-------------
-------------
-------------
0------------
-0-0---------
--0-0-----0--
---0-0---0-0-
--0---0-0-0-0
-0-------0-01
---1--0---001
-01100-0--10-
-0-2----01---
0101-0---00--
010---01---0-
-0----1-10--0
--0--0000----
---00110-----
-----00-0----
----0--0-0---
---0-00-0-0--
--0-0010-0-0-
-0-0-0100-0--
--0-0-00-0---
---0-0--0----
----0--------
-------------

*** Life<FredkinCell> 23x13 ***

Generation = 0, Population = 9.
-------------
-----------0-
--0----------
-------------
-------------
-------------
-------------
-------------
--------0----
---------0---
-0-------00--
-------------
-------------
-------------
-------------
-------------
-------------
-------------
-------------
-------------
------------0
-----------0-
-------------

Generation = 2, Population = 27.
--0----------
---------0---
0---0--------
-----------0-
--0----------
-------------
--------0----
---------0---
-0----0--0---
-------0---0-
---0---0---10
---------1---
-0-------00--
-------------
-------------
-------------
-------------
-------------
------------0
-----------0-
----------0-0
---------0---
------------0

Generation = 4, Population = 28.
--0----------
-------0-----
0-----0------
-------------
--------0----
---------0-0-
-00------00--
-------------
----0-------0
-----0-------
-0----1-----2
-------------
--------1----
---------1---
-0-------00--
-------------
------------0
-----------0-
-------------
-------------
--------0-0--
-------0-----
------------0

Generation = 6, Population = 55.
--0-0-0------
-----0---0---
0---0--------
-------0-0-0-
000------0---
-------0-2-0-
1--1---0---0-
-----0---2-0-
---------1---
---0---1-----
---0------1--
-----0---2---
---------0--0
-------0---0-
---0---0---1-
---------1-0-
-0-------0--0
---------0---
--------0-0-0
-------0---0-
------0-0-0--
-----0---0---
--------0----

Generation = 8, Population = 30.
--------0----
---0---0-0---
-1--0----0---
-------------
--2----------
-----------2-
-------------
-------------
0---------1--
-0-------2-1-
-0---1--0----
-------------
------------0
-----------2-
-------------
-------------
--------1----
---------2-0-
-0-------10-0
-------------
----0-0------
---0---0-----
-------------

*** Life<FredkinCell> 10x27 ***

Generation = 0, Population = 7.
---------------------------
--------0-----0------------
---------0-----------------
---------------------------
---------------------------
---------------------------
---------------------------
-------0-0-----------------
---------------------------
--------------0-0----------

Generation = 3, Population = 41.
------0-0---0---0----------
-----0-----0-0-0-0---------
--------------0-0----------
-----------0-0-0-----------
-------0-10---0------------
------0--10----------------
-----0-----0--0-0----------
----0-------00---0---------
-----0-----00-0-0-0--------
------0---00-0---0-0-------

Generation = 6, Population = 56.
-----0-0-0-0-0-------------
--0----101----0-----0------
---0---1-1-0---0-----------
----01-0-0100-0-0-0--------
-----0---2---0-------------
---0-001-100-0--0-0--------
-------0---1---------------
-0-0-0--0-10-320----0------
---------1-----------------
---0----0----1--------0----

Generation = 9, Population = 51.
0-----0-------0-------0----
-----0-2-----0-0-0---0-0---
--0---0-----------0---0----
-0-------1---0-------------
-------0-30-----0----------
------02--0--1-0-----------
-0-----100----00-0---------
0-0-----------2---0--------
-0----0-------00-0----0-0--
-----0-0-----1-0-----0---0-

*** Life<FredkinCell> 29x28 ***

Generation = 0, Population = 7.
----------------------------
----------------------------
----------0-----------------
---------------------0------
----------------------------
----------------------------
----------------------------
----------------------------
----------------------0-----
--------------0-------------
----------------------------
-----------0----------------
----------------------------
----------------------------
----------------------------
----------------------------
----------------------------
----------------------------
----------------------------
----------------------------
--------------0-------------
----------------------------
---------------------0------
----------------------------
----------------------------
----------------------------
----------------------------
----------------------------
----------------------------

Generation = 3, Population = 107.
---------0-0---------0------
--------0-0-0-------0-0-----
-------0-0-0-0-----0-0-0----
--------0-0-0-----0-0-0-0---
---------0-0-------0-0-0----
----------0---------0-------
--------------0--------0----
-------------0-0----0-0-0---
-----------00-0-0--0-0-0-0--
----------0110-0-0--0-0-0---
---------0-0000-0----0-0----
--------0-0-0110------0-----
---------0-0-00-------------
----------0-0---------------
-----------0----------------
----------------------------
----------------------------
--------------0-------------
-------------0-0------------
------------0-0-0----0------
-----------0-0-0-0--0-0-----
------------0-0-0--0-0-0----
-------------0-0--0-0-0-0---
--------------0----0-0-0----
--------------------0-0-----
---------------------0------
----------------------------
----------------------------
----------------------------

*** Life<FredkinCell> 24x12 ***

Generation = 0, Population = 4.
------------
----------0-
------------
------------
-----0------
------------
------------
------------
------------
------------
------------
------------
------------
------------
----0-------
------------
------------
------------
------------
------------
------------
------------
--0---------
------------

Generation = 3, Population = 51.
--------0---
-----0-0-0--
----0-0-0-0-
---0-0-0-0-0
--0-0-0-0-0-
---0-0-0----
----0-0-----
-----0------
------------
------------
------------
----0-------
---0-0------
--0-0-0-----
-0-0-0-0----
--0-0-0-----
---0-0------
----0-------
------------
--0---------
-0-0--------
0-0-0-------
-0-0-0------
0---0-------

Generation = 6, Population = 44.
---0-0-0----
----0---0---
-0---0---0--
------0---0-
---0---0---0
--------0---
-0---0---0--
----------0-
---01--0----
------------
--0--10-----
------------
0---0---0---
------------
0-0---0---0-
------------
0-0-0---0---
------------
0-0-0-0-----
------------
0---0-0-----
------------
--0-----0---
------------

*** Life<FredkinCell> 16x29 ***

Generation = 0, Population = 20.
0----------------------------
-----------0------0-0-0-----0
-----------------------------
-----------------------------
-----------------------------
-----------------------------
-----------------------------
-----------------------------
--0-----0--------------------
---------0-------------------
-0--------0------------------
------------------0----------
-----------------00----------
---------------------0-------
--------------------0--------
0-0-------0----0-------------

Generation = 2, Population = 57.
--0--------------------------
---------0---0--0-0---0-0-0-0
0----------------------------
-----------0------0-0-0-----0
-----------------------------
-----------------------------
--0-----0--------------------
---------0-------------------
01--0-0----------------------
-------0---0------0----------
--10--------0----01----------
---------0------0---00-------
-0--------0----01--1---------
0-0-------0----0--10---0-----
-----------------0----0------
0---0---0-0-00-0-0---0-------

Generation = 4, Population = 68.
--0-0------------------------
-------0---0--110---0-------0
0----------------------------
-----------------------------
0-0-----0--------------------
---------0-0------0-0-0-----0
-1--------0------------------
------------------0----------
2---0-0-----0----01----------
-----0-------0-------0-------
-1---00-------0-----1--------
0-1-------0---00------1------
--0-----0----01------10------
0-0------12----2-0-------0---
-0--------0-----1---0---0----
0-0-0------0--0---10-0-------

*** Life<FredkinCell> 7x5 ***

Generation = 0, Population = 17.
0----
00000
-0-00
0-0--
--000
0---0
-0---

Generation = 2, Population = 23.
-0111
0202-
12---
-1-10
01000
--1-2
-01-1

Generation = 4, Population = 18.
-0--3
--2--
--00-
1-2--
23222
11112
--1--
